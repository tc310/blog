package me.cycle.blog.config;

import me.cycle.blog.objects.Article;
import me.cycle.blog.objects.Tag;
import me.cycle.blog.objects.User;
import me.cycle.blog.route.AdminRute;
import me.cycle.blog.route.FrontRute;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;

public class BlogConfig  extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("classes/jdbc.properties");
		me.setDevMode(true);
	}

	public void configRoute(Routes me) {
		me.add(new FrontRute()) ;
		me.add(new AdminRute()) ;
	}

	public void configPlugin(Plugins me) {
	
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"),
		getProperty("username"), getProperty("password"));
		me.add(c3p0Plugin);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.addMapping("user", User.class);
		arp.addMapping("article", Article.class) ;
		arp.addMapping("tag", Tag.class) ;
		
	}

	public void configInterceptor(Interceptors me) {
		
	}

	public void configHandler(Handlers me) {
		
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("WebContent", 90, "/", 5);
	}
}
