package me.cycle.blog.util;

import org.apache.commons.lang.math.RandomUtils;

public class StringUtil {

	/**
	 * 把用逗号个来的字符串改为字符用单引号，用于数据库in查询用。
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	public static String FormatForDB(String id) throws Exception{
		if(strIsNotNull(id)){
			String str="";
			str=("'"+id+"'").replaceAll(",", "','");
			return str;
		}else{
			return null;
		}
	}

	
	/**
	 * 过滤掉字符串值中的部分符号，用于WHERE条件判断中将SQL或HQL语句直接连接字符串值的情况。
	 * 被过滤的符号如下
	 * '-->\',
	 * \"-->\\",
	 * (-->空格,
	 * )-->空格,
	 * =-->空格,
	 * ..-->空格,
	 * =-->空格 ,
	 * >-->空格 ,
	 * <-->空格,
	 * *-->空格 
	 * @param val
	 * @return
	 */
	public static String filterQueryValue(String val) {
		if (null == val)
			return "";
		
		String ret = val.replace("'", "\'");
		ret = ret.replace("\"", "\\\"").replace("(", "").replace(")", "").replace("*", "")
				.replace("=", "").replace("..", "").replace(">", "").replace("<", "");
		
		if(!ret.equals(val)){
			
		}
		return ret;
	}

	

	/**
	 * 把属性按照java的命名规范把下滑线进行转换
	 * @param str
	 * @return
	 */
	public static String changePropertyToJavaFormat(String str){
		String dealStr="";
		
		str=str.toLowerCase();
		
		String strs[]=str.split("_");
		
		if(strs.length>1){
			dealStr=strs[0];
			for(int i=1;i<strs.length;i++){
				dealStr=dealStr+strs[i].substring(0, 1).toUpperCase()+strs[i].substring(1,strs[i].length());
			}
		}else{
			dealStr=str;
		}
		
		return dealStr;
	}
	
	/**
	 * 生成get和set方法
	 * @param val
	 * @param type
	 * @return
	 */
	public static StringBuffer createGetSetMethod(String val,String type){
		   StringBuffer stringBuffer=new StringBuffer();
		   
		   //getMethod
		   stringBuffer.append("    public "+type+"  get"+toUpperFirstCase(val)+"()\n");
		   stringBuffer.append("     {\n");
		   stringBuffer.append("         return "+val+";\n");
		   stringBuffer.append("     }\n");
		   
		 //setMethod
		   stringBuffer.append("    public void  set"+toUpperFirstCase(val)+"("+type+" "+val+")\n");
		   stringBuffer.append("     {\n");
		   stringBuffer.append("        this."+val+"="+val+";\n");
		   stringBuffer.append("     }\n");   
		   stringBuffer.append("\n\n\n");
		   return stringBuffer;
	   }
	
  /**
   * 首字符大写
   * @param str
   * @return
   */
    public static String toUpperFirstCase(String str){
		
		str=str.substring(0,1).toUpperCase()+str.substring(1,str.length());
		
		return str;
	}
    
    /**
	 * 判断字符串任意多个是否全部为空
	 * @param str判断的字符串
	 * @return
	 * @author mzl
	 */
	public static boolean isAllEmpty(String ...strs){
		
		boolean flag=true;
		for(String str:strs){
			if(str!=null&&!"".equals(str.trim())){
				flag =false;
				break;
			}
		}
		return flag;
	}
	

	/**
	 * 判断字符串任意多个至少一个为空
	 * @param str判断的字符串
	 * @return
	 * @author mzl
	 */
	public static boolean isOneEmpty(String ...strs){
		
		boolean flag=false;
		for(String str:strs){
			if(null==str||"".equals(str.trim())){
				flag =true;
				break;
			}
		}
		
		return flag;
	}
	

	/**
	 * 创建固定位数的数字随机数
	 * @param length 随机数的长度
	 */
	
	public static String createRandomStr(int length){
		String str="";
		for(int i=0;i<length;i++){
			str+=RandomUtils.nextInt(10);
		}
		
		return str;
	}
	
	
	/**
	 * 创建固定位数的数字随机数
	 * @param length 随机数的长度
	 */
	
	public static int createRandomNumber(int length){
		String str="";
		for(int i=0;i<length;i++){
			str+=RandomUtils.nextInt(10);
		}
		int result=Integer.parseInt(str);
		return result;
	}
	
	
	/**
	 * 补充固定字符，让字符固定长度
	 * @param resource  源字符串
	 * @param repeatChar  补充字
	 * @param length  固定长度
	 * @return
	 */
	public static String makeUpStr(String resource,String repeatChar,int length){
		if(null==resource){
			return resource;
		}
		String str="";
		for(int i=0;i<length-resource.length();i++){
			str+=repeatChar;
		}
		return str+resource;
	}
	
	/**
	 * 判断string变量是否为空，非空返回true，空返回false
	 * @param strParam string变量
	 * @return
	 * @throws Exception
	 */
	public static Boolean strIsNotNull(String strParam)throws Exception{
		 if(null!=strParam&&!"".equals(strParam.trim())){
			 return true;
		 }else{
			return false; 
		 }
	}
	
	/**
	 * 简单防止XSS攻击，清除特殊字符
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public static String clearXSSHtml(String message)throws Exception{
		if(!isAllEmpty(message)){
			message = message.replace ('<',' '); 
			message = message.replace ('>',' '); 
			message = message.replace ('"',' ');
			message = message.replace ('\'',' ');
			message = message.replace ('/',' ');
			message = message.replace ('%',' '); 
			message = message.replace (';',' '); 
			message = message.replace ('(',' '); 
			message = message.replace (')',' '); 
			message = message.replace ('&',' '); 
			message = message.replace ('+','_'); 
		}
		return message;
	}
	
	
	/**
	 * 从XMLString中获取某一节点的值.
	 * 该节点必须唯一.否则只能获取首个和tag值相同的节点的值.
	 * @param tag节点名
	 * */
	public static String getDataFromXMLString(String tag,String XMLString){
		String data = null ;
		if(null!=tag || !"".equals(tag)){
			int begin = XMLString.indexOf("<"+tag+">") ,
					end = XMLString.indexOf("</"+tag+">") ;
			if(begin>0)data = XMLString.substring(begin+tag.length()+2,end) ;
			else data="error" ;
		}
		return data ;
	}
	
	

/**
 * 修改视频截图文件名
 * 酷六视频的截图有三种大小,文件名前加"10"为中 ;文件名前加"20"为大;不加,则为小. 
 * @param copPicName 视频截图文件名
 * @param str 增加到文件名的字符串.
 * */
public static String modifyCopPicName(String copPicName,String str){
	
	String result  = null ;
	if(null !=copPicName){
		result = copPicName.substring(0,
				copPicName.lastIndexOf("/") + 1)
					+ str
					+ copPicName.substring(copPicName.lastIndexOf("/") + 1,
							copPicName.lastIndexOf("."))
					+ copPicName.substring(copPicName.lastIndexOf("."));
	}
	return result ;
}

}
