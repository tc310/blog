package me.cycle.blog.util;

import java.security.MessageDigest;

/**
 * 该类是用于字符串的加密
 * 使用类：java.security.MessageDigest
 * @author 蔡熙伟
 */

public class CommonEncryption {
	
	//测试系统中一般使用常用密码，这里给出他们的值，以便查询
	
	/*测试常用密码：111
	 * MD5:698d51a19d8a121ce581499d7b701668
	 * SHA1+MD5:c72f5359811af711f29b1fac7150042f
	 * 
	 * SHA1:6216f8a75fd5bb3d5f22b6f9958cdede3fc086c2
	 * MD5+SHA1:3be0ff98032936bc7f9df51c5685ee5f2dd6ccee
	*/
	
	/*测试常用密码：123
	 * MD5:202cb962ac59075b964b07152d234b70
	 * SHA1+MD5:6116afedcb0bc31083935c1c262ff4c9
	 * 
	 * SHA1:40bd001563085fc35165329ea1ff5c5ecbdbbeef
	 * MD5+SHA1:adcd7048512e64b48da55b027577886ee5a36350
	*/
	
	/*测试常用密码：111111
	 * MD5:96e79218965eb72c92a549dd5a330112
	 * SHA1+MD5:c78b6663d47cfbdb4d65ea51c104044e
	 * 
	 * SHA1:3d4f2bf07dc1be38b20cd6e46949a1071f9d0e3d
	 * MD5+SHA1:d8406e8445cc99a16ab984cc28f6931615c766fc
	*/
	
	/*测试常用密码：123456
	 * MD5:e10adc3949ba59abbe56e057f20f883e
	 * SHA1+MD5:d93a5def7511da3d0f2d171d9c344e91
	 * 
	 * SHA1:7c4a8d09ca3762af61e59520943dc26494f8941b
	 * MD5+SHA1:10470c3b4b1fed12c3baac014be15fac67c6e815
	*/
	
	/**
	 * MD5+SHA1加密(先MD5再SHA1加密)
	 * @param Str	[String]需要机密的字符串
	 * @return resultStr [String]加密后字符串字符串
	 * @throws Exception 加载包会抛出异常
	 */
	public String encryptByMD5toSHA1(String str)throws Exception{
		String resultStr = str;
		resultStr = this.encryptByMD5_32Bit(resultStr);
		resultStr = this.encryptBySHA1(resultStr);
		return resultStr;
	}
	
	/**
	 * SHA1+MD5加密(先SHA1再MD5加密)
	 * @param Str	[String]需要机密的字符串
	 * @return resultStr [String]加密后字符串字符串
	 * @throws Exception 加载包会抛出异常
	 */
	public String encryptBySHA1toMD5(String str)throws Exception{
		String resultStr = str;
		resultStr = this.encryptBySHA1(resultStr);
		resultStr = this.encryptByMD5_32Bit(resultStr);
		return resultStr;
	}
	
	/**
	 * MD5加密(32位)
	 * @param Str	[String]需要机密的字符串
	 * @return resultStr [String]加密后字符串字符串32位
	 * @throws Exception 加载包会抛出异常
	 */
	public String encryptByMD5_32Bit(String Str) throws Exception{
		MessageDigest md5 = null;
		md5 = MessageDigest.getInstance("MD5");
		char[] charArray = Str.toCharArray();
		byte[] byteArray = new byte[charArray.length];
		for (int i = 0,len = charArray.length; i < len; i++){
			byteArray[i] = (byte) charArray[i];
		}
		
		byte[] md5Bytes = md5.digest(byteArray);
		StringBuffer resultStr = new StringBuffer();
		for (int i = 0,len = md5Bytes.length; i < len; i++) {
			int val = ((int)md5Bytes[i]) & 0xff;
			if (val < 16) {
				resultStr.append("0");
			}
			resultStr.append(Integer.toHexString(val));
		}
		return resultStr.toString();
	}
	
	/**
	 * SHA1加密
	 * @param Str	[String]需要机密的字符串
	 * @return resultStr [String]加密后字符串字符串
	 * @throws Exception 加载包会抛出异常
	 */
	public String encryptBySHA1(String str) throws Exception{
	    MessageDigest sha = MessageDigest.getInstance("SHA");  
	    sha.update(str.getBytes("utf8"));
	    byte [] bytes=sha.digest();
	    StringBuilder returnStr=new StringBuilder(bytes.length<<1);
	    for(int i=0;i<bytes.length;i++){
	    	returnStr.append(Character.forDigit((bytes[i]>>4)&0xf,16));
	    	returnStr.append(Character.forDigit(bytes[i]&0xf,16));
	    }
	    return returnStr.toString();
	}
	

}
