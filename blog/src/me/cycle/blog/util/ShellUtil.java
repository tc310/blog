package me.cycle.blog.util;

import java.io.InputStreamReader;
import java.io.LineNumberReader; 

public class ShellUtil {

	/**
	 * 通过iptables 禁止ip访问.
	 * @param ip 需要禁止访问的ip.
	 * @author ckf
	 * */
	public void forbidIP(String ip){
		
	}
	
	/**
	 * 
	 * */
	public static Object exec(String cmd) {
		try {
			String[] cmdA = { "/bin/sh", "-c", cmd };
			Process process = Runtime.getRuntime().exec(cmdA);
			LineNumberReader br = new LineNumberReader(new InputStreamReader(
					process.getInputStream()));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				sb.append(line).append("\n");
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
