package me.cycle.blog.controller;

import java.util.Date;

import me.cycle.blog.objects.Article;
import me.cycle.blog.validator.BlogValidator;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

public class AdminController extends Controller{
	
	public void index(){
		setAttr("blogPage", Article.articleDao.paginate(getParaToInt(0, 1), 10, "select *", "from article order by id"));
		render("index.html");
	}
	
	public void toAdd(){
		render("articleAdd.html"); 
	}
	
//	@Before(BlogValidator.class)
	public void add(){
		try {
			Article article = new  Article() ;
			article.set("title", getPara("title") ) ;
			article.set("cre_time", new Date() ) ;
			article.save() ;
			redirect("/admin");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
