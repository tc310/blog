package me.cycle.blog.route;

import me.cycle.blog.controller.FrontController;

import com.jfinal.config.Routes;

public class FrontRute extends Routes {

	@Override
	public void config() {
		// TODO Auto-generated method stub
		add("/", FrontController.class);//首页
		add("/article", FrontController.class); //文章列表
	}

}
